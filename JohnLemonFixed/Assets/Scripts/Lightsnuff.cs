﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightsnuff : MonoBehaviour
{
    public GameObject light;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            light.SetActive(false);
        }
    }

}
