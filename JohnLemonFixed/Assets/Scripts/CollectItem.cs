﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectItem : MonoBehaviour
{
    public GameObject objectToBeDisabled;
     void OnTriggerEnter(Collider other)
    {
        if (!objectToBeDisabled.activeSelf)
        {
            ScoringSystem.theScore += 1;
            Destroy(gameObject);
        }
    }
}
